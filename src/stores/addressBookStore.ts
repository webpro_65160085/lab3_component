import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
  id: number
  name: string
  tel: string
  gender: string
}
export const useAddressBookStore = defineStore('address-book', () => {

  let lastId = 1
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  })
  const addressList = ref<AddressBook[]>([])
  const isAddnew = ref(false)
  function save() {
    if (address.value.id > 0) {
      // Edit
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    } else {
      // Add new
      addressList.value.push({ ...address.value, id: lastId++ })
    }
    isAddnew.value = false
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }
  function edit(id: number) {
    isAddnew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
    // Copy Object JSON.parse(JSON.stringify(object))
  }
  function remove(id: number) {
    const removedIndex = addressList.value.findIndex((item) => item.id === id)
    // Copy Object JSON.parse(JSON.stringify(object))
    addressList.value.splice(removedIndex, 1)
  }
  function cancel() {
    isAddnew.value = false
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }

  return { address, addressList, save, isAddnew, edit, remove, cancel }
})
